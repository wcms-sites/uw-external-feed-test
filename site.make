core = 7.x
api = 2

;uw_web_tokens
projects[uw_web_tokens][type] = "module"
projects[uw_web_tokens][download][type] = "git"
projects[uw_web_tokens][download][url] = "https://git.uwaterloo.ca/wcms/uw_web_tokens.git"
projects[uw_web_tokens][download][tag] = "7.x-1.0"
projects[uw_web_tokens][subdir] = ""

; uw_theme_marketing
projects[uw_theme_marketing][type] = "theme"
projects[uw_theme_marketing][download][type] = "git"
projects[uw_theme_marketing][download][url] = "https://git.uwaterloo.ca/wcms/uw_theme_marketing.git"
projects[uw_theme_marketing][download][branch] = "MSI-instagram-feed"

; uw_ct_single_page_home
projects[uw_ct_single_page_home][type] = "module"
projects[uw_ct_single_page_home][download][type] = "git"
projects[uw_ct_single_page_home][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_single_page_home.git"
projects[uw_ct_single_page_home][download][branch] = "MSI-instagram-feed"
projects[uw_ct_single_page_home][subdir] = ""
